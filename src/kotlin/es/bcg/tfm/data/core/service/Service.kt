//package es.openbank.rooms.common.data.core.service
//
//import io.ktor.client.HttpClient
//import io.ktor.client.call.receive
//import io.ktor.client.request.HttpRequestBuilder
//import io.ktor.client.request.header
//import io.ktor.client.request.request
//import io.ktor.client.response.HttpResponse
//import io.ktor.http.HttpMethod
//import io.ktor.http.HttpStatusCode
//import io.ktor.http.takeFrom
//import kotlinx.io.core.use
//
//internal abstract class Service {
//
//    companion object {
//        private const val HEADER_TOKEN = "token"
//        private const val HEADER_CONTENT_TYPE = "Content-Type"
//        private const val HEADER_CONTENT_TYPE_JSON = "application/json"
//        private const val HEADER_AUTHORIZATION = "Authorization"
//        private const val HEADER_AUTHORIZATION_BASIC = "Basic am9zZWp1LnJhbWlyZXpAZ3J1cG9zYW50YW5kZXIuY29tOmFkbWlu"
//    }
//
//    protected abstract fun getEndPoint(): String
//
//    protected abstract fun getClient(): HttpClient
//
//    protected suspend inline fun <reified T> requestLogin(
//        _method: HttpMethod,
//        path: String,
//        _body: Any? = null
//    ): Call<T> =
//        try {
//            getClient().request<HttpResponse> {
//                header(HEADER_AUTHORIZATION, HEADER_AUTHORIZATION_BASIC)
//                apiUrl(path)
//                method = _method
//                _body?.let {
//                    header(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_JSON)
//                    body = it
//                }
//            }.use {
//                Call(it.status, it.receive())
//            }
//        } catch (e: Throwable) {
//            Call(HttpStatusCode.BadRequest, null, e.toString())
//        }
//
//    protected suspend inline fun <reified T> request(
//        _method: HttpMethod,
//        path: String,
//        token: String? = null,
//        _body: Any? = null
//    ): Call<T> =
//        try {
//            getClient().request<HttpResponse> {
//                token?.let {
//                    header(HEADER_TOKEN, token)
//                }
//                apiUrl(path)
//                method = _method
//                _body?.let {
//                    header(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_JSON)
//                    body = it
//                }
//            }.use {
//                Call(it.status, it.receive())
//            }
//        } catch (e: Throwable) {
//            Call(HttpStatusCode.BadRequest, null, e.toString())
//        }
//
//    protected fun HttpRequestBuilder.apiUrl(path: String) {
//        url {
//            takeFrom(getEndPoint())
//            encodedPath = path
//        }
//    }
//}