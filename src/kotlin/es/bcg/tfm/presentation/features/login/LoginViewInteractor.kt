package es.openbank.rooms.common.presentation.features.login

import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.model.user.UserModel
import es.openbank.rooms.common.presentation.core.PresentationContract

interface LoginViewInteractor : PresentationContract.ViewInteractor {

    fun loginUser(userId: String, password: String)

    fun showLoginUserSuccess(userModel: UserModel)

    fun showLoginUserError(error: FailureType)
//}