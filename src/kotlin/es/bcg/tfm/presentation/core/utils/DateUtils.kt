package es.openbank.rooms.common.presentation.core.utils

internal fun Int.minutesToMilliseconds(): Long = (this * 60000).toLong()