//package es.openbank.rooms.common.data.entity.login.response
//
//import es.openbank.rooms.common.domain.model.user.UserModel
//import kotlinx.serialization.Serializable
//
//@Serializable
//data class LoginResponse(
//    val token: String
//) {
//
//    companion object {
//        fun transform(loginResponse: LoginResponse) =
//            UserModel(loginResponse.token)
//    }
//}