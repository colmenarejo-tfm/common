package es.openbank.rooms.common.data.service.login

import es.openbank.rooms.common.data.core.service.Call
import es.openbank.rooms.common.data.core.service.Service
import es.openbank.rooms.common.data.entity.login.request.LoginRequest
import es.openbank.rooms.common.data.entity.login.response.LoginResponse
import io.ktor.client.HttpClient
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.json.serializer.KotlinxSerializer
import io.ktor.http.HttpMethod

internal class LoginServiceImpl : LoginService, Service() {

    companion object {
        private const val END_POINT = "http://212.71.255.248:9000"
        private const val PATH_LOGIN = "/login"
    }

    override fun getEndPoint(): String = END_POINT

    override fun getClient(): HttpClient = HttpClient {
        install(JsonFeature) {
            serializer = KotlinxSerializer().apply {
                setMapper(LoginRequest::class, LoginRequest.serializer())
                setMapper(LoginResponse::class, LoginResponse.serializer())
            }
        }
    }

    override suspend fun postLogin(body: LoginRequest): Call<LoginResponse> =
        requestLogin(HttpMethod.Post, PATH_LOGIN, body)
}