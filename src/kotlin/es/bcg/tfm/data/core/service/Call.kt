//package es.openbank.rooms.common.data.core.service
//
//import io.ktor.http.HttpStatusCode
//
//internal class Call<T>(
//    val statusCode: HttpStatusCode,
//    val body: T?,
//    val error: String? = null
//)