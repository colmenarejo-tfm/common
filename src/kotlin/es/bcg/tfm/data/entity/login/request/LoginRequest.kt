//package es.openbank.rooms.common.data.entity.login.request
//
//import kotlinx.serialization.Serializable
//
//@Serializable
//data class LoginRequest(
//    val clientId: String,
//    val password: String
//) {
//
//    companion object {
//        fun transform(clientId: String, password: String) = LoginRequest(clientId, password)
//    }
//}