package es.openbank.rooms.common.domain.core.usecase

interface UseCaseContract {

    fun cancel()
}