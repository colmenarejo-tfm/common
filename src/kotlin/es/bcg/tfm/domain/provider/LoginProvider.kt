package es.openbank.rooms.common.domain.provider

import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.core.functional.Either
import es.openbank.rooms.common.domain.model.user.UserModel

internal interface LoginProvider {

    suspend fun loginUser(userId: String, password: String): Either<FailureType, UserModel>
}