package es.openbank.rooms.common.data.service.login

import es.openbank.rooms.common.data.core.service.Call
import es.openbank.rooms.common.data.entity.login.request.LoginRequest
import es.openbank.rooms.common.data.entity.login.response.LoginResponse

internal interface LoginService {

    suspend fun postLogin(body: LoginRequest): Call<LoginResponse>
}