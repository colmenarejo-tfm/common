//package es.openbank.rooms.common.data.local
//
//import es.openbank.rooms.common.domain.model.room.RoomModel
//
//class LocalDataImpl : LocalData {
//
//    private var token: String? = null
//    private var roomList: MutableList<RoomModel>? = null
//
//    override fun getToken(): String = token ?: ""
//
//    override fun setToken(token: String) {
//        this.token = token
//    }
//
//    override fun setRooms(roomList: List<RoomModel>) {
//        this.roomList = roomList.toMutableList()
//    }
//
//    override fun getRooms(): List<RoomModel>? = roomList
//
//    override fun getRoomName(roomId: Int): String =
//            roomList?.filter { it.id == roomId }?.get(0)?.name ?: ""
//}