//package es.openbank.rooms.common.data.local
//
//import es.openbank.rooms.common.domain.model.room.RoomModel
//
//interface LocalData {
//
//    fun setToken(token: String)
//
//    fun getToken(): String
//
//    fun setRooms(roomList: List<RoomModel>)
//
//    fun getRooms(): List<RoomModel>?
//
//    fun getRoomName(roomId: Int): String
//}