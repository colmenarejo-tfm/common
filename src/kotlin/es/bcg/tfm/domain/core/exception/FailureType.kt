package es.bcg.tfm.domain.core.exception

sealed class FailureType {
    abstract class FeatureFailure : FailureType()

    object NetworkConnection : FailureType()
    data class ServerError(val message: String) : FailureType()
    object LocalError : FailureType()
    object TokenError : FailureType()
}