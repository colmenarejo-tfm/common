package es.openbank.rooms.common.data.provider.login

import es.openbank.rooms.common.data.core.provider.NetworkProvider
import es.openbank.rooms.common.data.entity.login.request.LoginRequest
import es.openbank.rooms.common.data.entity.login.response.LoginResponse
import es.openbank.rooms.common.data.local.LocalData
import es.openbank.rooms.common.data.service.login.LoginService
import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.core.functional.Either
import es.openbank.rooms.common.domain.model.user.UserModel
import es.openbank.rooms.common.domain.provider.LoginProvider

internal class LoginProviderImpl(
    private val loginService: LoginService,
    private val localData: LocalData
) : LoginProvider, NetworkProvider() {

    override suspend fun loginUser(userId: String, password: String): Either<FailureType, UserModel> =
        request(loginService.postLogin(LoginRequest.transform(userId, password))) {
            val userModel = LoginResponse.transform(it)
            localData.setToken(userModel.token)
            userModel
        }
}