package es.openbank.rooms.common.presentation.features.login

import es.openbank.rooms.common.data.local.LocalData
import es.openbank.rooms.common.data.provider.login.LoginProviderImpl
import es.openbank.rooms.common.data.service.login.LoginService
import es.openbank.rooms.common.data.service.login.LoginServiceImpl
import es.openbank.rooms.common.domain.core.usecase.Executor
import es.openbank.rooms.common.domain.provider.LoginProvider
import es.openbank.rooms.common.domain.usecase.login.PostLoginUserUseCase

class LoginDependency(private val executor: Executor, private val localData: LocalData) {

    fun providePresenter(): LoginPresenter =
        LoginPresenter(providePostLoginUseCase(executor, localData))

    internal fun providePostLoginUseCase(executor: Executor, localData: LocalData) =
        PostLoginUserUseCase(provideLoginProvider(localData), executor)

    internal fun provideLoginProvider(localData: LocalData): LoginProvider =
        LoginProviderImpl(provideLoginService(), localData)

    internal fun provideLoginService(): LoginService = LoginServiceImpl()
}