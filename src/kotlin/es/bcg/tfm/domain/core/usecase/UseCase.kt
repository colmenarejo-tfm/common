package es.openbank.rooms.common.domain.core.usecase

import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.core.functional.Either
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

internal abstract class UseCase<out T, in Params>(private val executor: Executor) :
    UseCaseContract where T : Any {

    private var job: Job? = null

    abstract suspend fun run(params: Params): Either<FailureType, T>

    operator fun invoke(onResult: (Either<FailureType, T>) -> Unit, params: Params) {
        job = GlobalScope.launch(context = executor.main) { onResult(run(params)) }
    }

    override fun cancel() {
        job?.cancel()
    }

    object None
}