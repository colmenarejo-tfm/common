package es.openbank.rooms.common.domain.usecase.login

import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.core.functional.Either
import es.openbank.rooms.common.domain.core.usecase.Executor
import es.openbank.rooms.common.domain.core.usecase.UseCase
import es.openbank.rooms.common.domain.model.user.UserModel
import es.openbank.rooms.common.domain.provider.LoginProvider

internal class PostLoginUserUseCase(
    private val loginProvider: LoginProvider,
    executor: Executor
) : UseCase<UserModel, PostLoginUserUseCase.Params>(executor) {

    override suspend fun run(params: Params): Either<FailureType, UserModel> =
        loginProvider.loginUser(params.userId, params.password)

    data class Params(val userId: String, val password: String)
}