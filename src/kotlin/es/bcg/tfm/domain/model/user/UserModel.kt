package es.openbank.rooms.common.domain.model.user

data class UserModel(
    val token: String
)