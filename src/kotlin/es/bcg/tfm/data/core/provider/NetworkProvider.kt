//package es.openbank.rooms.common.data.core.provider
//
//import es.openbank.rooms.common.data.core.service.Call
//import es.openbank.rooms.common.domain.core.exception.FailureType
//import es.openbank.rooms.common.domain.core.exception.FailureType.ServerError
//import es.openbank.rooms.common.domain.core.functional.Either
//import es.openbank.rooms.common.domain.core.functional.Either.Left
//import es.openbank.rooms.common.domain.core.functional.Either.Right
//import io.ktor.http.HttpStatusCode
//
//@Suppress("UNCHECKED_CAST")
//internal open class NetworkProvider {
//
//    inline fun <T, R> request(call: Call<T>, transform: (T) -> R): Either<FailureType, R> {
//        return try {
//            when (call.statusCode) {
//                HttpStatusCode.OK -> Right(transform(call.body as T))
//                HttpStatusCode.Unauthorized -> Left(FailureType.TokenError)
//                else -> Left(ServerError("${call.statusCode} -> ${call.error}"))
//            }
//        } catch (exception: Throwable) {
//            Left(ServerError(exception.toString()))
//        }
//    }
//}