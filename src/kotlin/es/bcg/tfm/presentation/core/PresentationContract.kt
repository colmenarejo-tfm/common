package es.openbank.rooms.common.presentation.core

import es.openbank.rooms.common.domain.core.exception.FailureType

interface PresentationContract {

    interface Presenter<in VI : ViewInteractor> {

        fun attachInteractor(viewInteractor: VI)

        fun initialize() {}

        fun destroy()
    }

    interface ViewInteractor {

        fun initialize()

        fun destroy()

        fun showError(error: FailureType)

        fun logOut()
    }
}