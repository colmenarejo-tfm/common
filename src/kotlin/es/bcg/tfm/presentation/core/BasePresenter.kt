package es.openbank.rooms.common.presentation.core

import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.core.usecase.UseCaseContract

abstract class BasePresenter<T : PresentationContract.ViewInteractor>(
    private vararg val useCaseList: UseCaseContract
) : PresentationContract.Presenter<T> {

    protected var mViewInteractor: T? = null

    override fun attachInteractor(viewInteractor: T) {
        mViewInteractor = viewInteractor
        mViewInteractor?.initialize()
    }

    override fun destroy() {
        for (useCase in useCaseList) {
            useCase.cancel()
        }
        mViewInteractor = null
    }

    protected open fun handleError(error: FailureType) {
        mViewInteractor?.showError(error)
    }

    protected fun checkError(error: FailureType) {
        if (error is FailureType.TokenError) {
            mViewInteractor?.logOut()
            return
        }
    }
}