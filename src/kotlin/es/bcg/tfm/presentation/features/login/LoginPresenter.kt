package es.openbank.rooms.common.presentation.features.login

import es.openbank.rooms.common.domain.core.exception.FailureType
import es.openbank.rooms.common.domain.model.user.UserModel
import es.openbank.rooms.common.domain.usecase.login.PostLoginUserUseCase
import es.openbank.rooms.common.presentation.core.BasePresenter

class LoginPresenter : BasePresenter<LoginViewInteractor>(postLoginUserUseCase) {

    internal constructor(private val postLoginUserUseCase: PostLoginUserUseCase)

    fun loginUser(userId: String, password: String) {
        postLoginUserUseCase(
            { it.either(::handleError, ::handleLoginSuccess) },
            PostLoginUserUseCase.Params(userId, password)
        )
    }

    private fun handleLoginSuccess(userModel: UserModel) {
        mViewInteractor?.showLoginUserSuccess(userModel)
    }

    override fun handleError(error: FailureType) {
        super.handleError(error)
        when (error) {
            is FailureType.TokenError -> mViewInteractor?.logOut()
            else -> mViewInteractor?.showLoginUserError(error)
        }
    }
}