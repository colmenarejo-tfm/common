package es.openbank.rooms.common.domain.core.usecase

import kotlinx.coroutines.CoroutineDispatcher

interface Executor {

    val main: CoroutineDispatcher
}